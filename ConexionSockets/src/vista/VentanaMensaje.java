package vista;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class VentanaMensaje extends JFrame implements ActionListener{
	
	static JTextField mensajeTexto;
	static JButton ok;
	JFrame f = new JFrame();
	int puerto;
	
	public void Formulario(String mensaje){
		System.out.println("Mensaje recibido");
		f.setTitle("Mensaje Recibido");
		f.setPreferredSize(new Dimension(400, 100));
		f.setLocation(450, 200);
        f.getContentPane().setLayout(new FlowLayout());
        mensajeTexto = new JTextField(mensaje);
        mensajeTexto.setEditable(false);
        ok=new JButton("OK");
		
		f.getContentPane().add(mensajeTexto);
        f.getContentPane().add(ok);
        ok.addActionListener(this);
        
       
        f.pack();
        f.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		
		if (ev.getSource()==ok) {
			this.f.setVisible(false);
		}
		
	}


	public int getPuerto() {
		return puerto;
	}

	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}
	
	
	

}
