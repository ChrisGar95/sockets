package vista;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import sockets.conector;

public class VentanaIngresar extends JFrame implements ActionListener{
	
	static JTextField mensajeTexto, puertoTexto;
	static JButton ingresar;
	int puerto;
	JFrame f = new JFrame();
	conector socket = new conector();
	
	VentanaMensaje VM = new VentanaMensaje();
	
	public void Formulario(){
		f.setTitle("Ingresar Puerto");
		f.setPreferredSize(new Dimension(400, 100));
		f.setLocation(450, 200);
        f.getContentPane().setLayout(new FlowLayout());
        puertoTexto = new JTextField("",5);
        ingresar=new JButton("Ingresar");
		
		f.getContentPane().add(puertoTexto);
        f.getContentPane().add(ingresar);
        ingresar.addActionListener(this);
        
       
        f.pack();
        f.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		
		if (ev.getSource()==ingresar) {
			
			this.f.setVisible(false);
			
			this.puerto=Integer.parseInt(this.puertoTexto.getText());
			this.socket.iniciar(puerto);
			
			
		
		}
		
	}


	public int getPuerto() {
		return puerto;
	}

	public void setPuerto(int puerto) {
		this.puerto = puerto;
	}
	
	
	

}
