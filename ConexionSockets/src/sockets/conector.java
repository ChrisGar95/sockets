package sockets;
import java.net.*;

import javax.swing.JFrame;
import javax.swing.JTextField;

import vista.VentanaMensaje;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.*;

public class conector {
	
	ServerSocket server;
	Socket socket;
	DataOutputStream salida;
	BufferedReader entrada;
	
	VentanaMensaje VM = new VentanaMensaje();
	
	public void iniciar(int puerto)
	{
	
		try 
		{
			server= new ServerSocket(puerto);
			socket = new Socket();
			socket = server.accept();
			
			entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			String mensaje = entrada.readLine();
			//System.out.println(mensaje);
			
			salida = new DataOutputStream(socket.getOutputStream());
			salida.writeUTF("Se ha enviado el mensaje "+mensaje);
			
			socket.close();
			
			this.VM.Formulario(mensaje);
			
			
		}catch(Exception e) {};
		
	}
	
	
}
